# Pymove

Python implementation of multi-objective environments and algorithms


# Installation

- Create the conda environment by running `conda env create -f environment.yml` (this might require admin privilege)
- Activate the environment `conda activate pymove`
- Run `python install.py`
- if you are on Windows also run `pip install windows-curses`

# Usage

```python
# setup a gym environment with vectorial rewards
env = gym.make('breakable-bottles-v0')

# setup a MORL algorithm
weighted_scalarize = WeightedScalarization() # weighs every objective equally
order_op = ScalarizingOrderOperator(weighted_scalarize)
exp_policy = EpsilonGreedy(env, epsilon=0.05, decay_params={"decay_rate": 0.99}, order_operation=order_op)
tq_algo = TabularQLearning(env, alpha=0.01, num_episodes=100, exploration_policy=exp_policy)

# learn value functions/policies
tq_algo.learn()
```

# Roadmap

algorithms:
- MIN as ordering functions

environments:
- [x] setup vectorial rewards for ai safety grid worlds
- [x] make MO ai safety gridworlds safe-grid-gym compatible
- [x] provide access to different ai safety gridworlds via the gym env version
- add Doors environment
- add valid observation_space, action_space and reward_space to safe_gym_grid
- optimize observation spaces by excluding walls etc.

other:
- make a pygame render function for breakable_bottles