from setuptools import setup

setup(
    name='pymove',
    version='0.0.1',
    keywords='multi-objective, environments, agents, rl, gym',
    url='',
    description='',
    packages=['pymove'],
    install_requires=[
        'gym',
        'numpy',
        'numba',
        'matplotlib'
    ]
)