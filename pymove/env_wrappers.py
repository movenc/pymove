import gym.spaces as spaces
from gym import Wrapper, RewardWrapper
import numpy as np

class PastReturnAugmented(Wrapper):
    """ Tracks the past return, based on a given discount factor and augments the observation by this return"""
    def __init__(self, env, discount_past):
        super().__init__(env)
        self.discount_past = discount_past
        # track past return for each objective
        num_objectives = env.reward_space.shape[0]
        self.past_return = np.zeros(num_objectives)
        # bounds of the reward values, based on geometric sum to -infinity
        self.min_past_return = env.reward_space.low/(1 - self.discount_past)
        self.max_past_return = env.reward_space.high/(1 - self.discount_past)
        # define wrapper observation_space
        self.observation_space = spaces.Dict({"obs": env.observation_space,
                                            "past_return": spaces.Box(self.min_past_return, self.max_past_return)})
    
    def reset(self):
        obs = self.env.reset()
        return (obs, self.past_return)
    
    def step(self, action):
        obs, reward, done, info = self.env.step(action)
        self.past_return = self.discount_past*self.past_return + reward
        return (obs, self.past_return), reward, done, info

class ObjectiveSelector(RewardWrapper):
    """ Returns only a selected subset of a multi-objective environment """
    def __init__(self, env, selected_objectives=None):
        super().__init__(env)
        self.selected_objectives = selected_objectives
        if selected_objectives is not None:
            # check whether each selected objective is only selected once
            assert len(set(selected_objectives)) == len(selected_objectives)
            # if a Box reward space is given adjust its dimension to the selection
            if hasattr(env, 'reward_space') and isinstance(env.reward_space, spaces.Box):
                self.reward_space = spaces.Box(env.reward_space.low[self.selected_objectives], env.reward_space.high[self.selected_objectives])
            self.num_objectives = len(selected_objectives)
    
    def reward(self, reward):
        if self.selected_objectives is not None:
            reward = np.array(reward)[self.selected_objectives]
        return reward