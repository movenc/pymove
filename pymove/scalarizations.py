import numpy as np

class WeightedScalarization():
    def __init__(self, weights=None, utility=None):
        self.weights = weights
        self.utility_function = utility

    def __call__(self, vec):
        if isinstance(vec, list):
            n = len(vec)
            vec = np.array(vec)
        elif isinstance(vec, np.ndarray):
            n = vec.shape[0]

        # define weights
        if self.weights is None or self.weights.shape[0] != n:
            self.weights = np.ones(n)/n

        # apply scalarization
        if self.utility_function is None:
            scalarized = self.weights.dot(vec)
        else:
            scalarized = self.weights.dot(self.utility_function(vec))
        return scalarized

# non-linear utility functions
def ELA(x):
    return -np.exp(-x)
vELA = np.vectorize(ELA)

def SFLLA(x):
    return np.where(x < 0, 1 - np.exp(-x), np.log(1 + x*(x >= 0)))
vSFLLA = np.vectorize(SFLLA)
