import numpy as np
from gym.spaces import flatdim
from pymove.exploration import EpsilonGreedy


class TabularQLambda:
    def __init__(self, env, num_episodes=1, alpha=0.1, gamma=1., lamb=0.95, exploration_policy=None):
        self.env = env
        self.num_episodes = num_episodes
        self.alpha = alpha
        self.gamma = gamma
        self.lamb = lamb

        if exploration_policy is None:
            self.exploration_policy = EpsilonGreedy(self.env)
        else:
            self.exploration_policy = exploration_policy

        # containers
        self.q_table = np.zeros((self.env.num_objectives, self.env.num_observations, self.env.num_actions))
        self.eligibility_trace = np.zeros((self.env.num_observations, self.env.num_actions))
        self.returns = np.zeros((self.env.num_objectives, self.num_episodes))

    
    def learn(self, return_trajectories=False):
        trajectories = [[] for _ in range(self.num_episodes)]
        # learn for num_episodes
        for eps in range(self.num_episodes):
            # initialize episode
            self.eligibility_trace[:, :] = 0.
            done = False
            obs = self.env.reset()
            obs_idx = self.env.get_obs_idx(obs)
            while not done:
                # select action
                #value_vectors = [self.q_table[:, obs_idx, a] for a in range(self.env.num_actions)]
                greedy_action = self.exploration_policy.select_greedy(self.q_table[:, obs_idx, :].squeeze())
                explore_action = self.exploration_policy.select(self.q_table[:, obs_idx, :].squeeze())
                # take action
                emission = self.env.step(explore_action)
                if return_trajectories:
                    trajectories[eps].append(emission)
                obs_prime, r, done, info = emission

                # keep track of past cumulative rewards
                self.returns[:, eps] += self.gamma**eps*np.array(r)

                if not done: # we do not maintain value for terminal states
                    obs_prime_idx = self.env.get_obs_idx(obs_prime)
                    # update state-action values
                    td_error = np.array(r)[:, None] + self.gamma * self.q_table[:, obs_prime_idx, greedy_action] - self.q_table[:, obs_idx, explore_action]
                    self.eligibility_trace[obs_idx, explore_action] += 1

                    for obs_temp in range(self.env.num_observations):
                        for action_temp in range(self.env.num_actions):
                            self.q_table[:, obs_temp, action_temp] += self.alpha*np.squeeze(td_error)*self.eligibility_trace[obs_temp, action_temp]
                            if greedy_action == explore_action:
                                self.eligibility_trace[obs_temp, action_temp] *= self.lamb*self.gamma
                            else:
                                self.eligibility_trace[obs_temp, action_temp] = 0.
                    obs_idx = obs_prime_idx

        if return_trajectories:
            return trajectories


class TabularQLearning:
    def __init__(self, env,
                num_episodes=1,
                alpha=0.1,
                alpha_decay=1.,
                gamma=1.,
                exploration_policy=None):
        self.env = env
        self.num_episodes = num_episodes
        self.initial_alpha = alpha
        self.alpha_decay = alpha_decay
        self.gamma = gamma

        if exploration_policy is None:
            self.exploration_policy = EpsilonGreedy(self.env)
        else:
            self.exploration_policy = exploration_policy

        # initialize Q tables (one for each objective)
        self.q_table = np.zeros((self.env.num_objectives, self.env.num_observations, self.env.num_actions))
        self.returns = np.zeros((self.env.num_objectives, self.num_episodes))

    def learn(self, verbose=False, return_trajectories=False):
        trajectories = [[] for _ in range(self.num_episodes)]
        # learn for num_episodes
        for eps in range(self.num_episodes):
            if verbose:
                print(f"Episode {eps} | epsilon {self.exploration_policy.epsilon}")
            # initialize episode
            alpha = self.initial_alpha
            done = False
            obs = self.env.reset()
            obs_idx = self.env.get_obs_idx(obs)
            while not done:
                # select action
                action = self.exploration_policy.select(self.q_table[:, obs_idx, :].squeeze())
                if verbose:
                    print(f"Action {action}")
                # take action
                emission = self.env.step(action)
                if return_trajectories:
                    trajectories[eps].append(emission)
                obs_prime, r, done, info = emission

                # keep track of past cumulative rewards
                self.returns[:, eps] += self.gamma**eps*np.array(r)

                if not done: # do not maintain value for terminal states
                    obs_prime_idx = self.env.get_obs_idx(obs_prime)
                    # update state-action values
                    td_error = np.array(r)[:,None] + self.gamma * np.max(self.q_table[:, obs_prime_idx, :], axis=2) - self.q_table[:, obs_idx, action]
                    self.q_table[:, obs_idx, action] += alpha*td_error
                    obs_idx = obs_prime_idx

                    # update learning rate
                    alpha *= self.alpha_decay
        if return_trajectories:
            return trajectories


if __name__ == "__main__":
    # test tabular Q-learning
    import matplotlib.pyplot as plt
    import gym
    import mo_gym

    from pymove.scalarizations import WeightedScalarization, ELA, SFLLA, vELA, vSFLLA
    from pymove.order_operators import ScalarizingOrderOperator, LexicographicOrderOperator
    from pymove.exploration import SoftmaxExploration

    import time
    
    bb_env = gym.make("breakable-bottles-v0")

    num_episodes = 100

    # Linear
    #lin_expp = SoftmaxExploration(bb_env, ranking_method="tournament")
    lin_expp = EpsilonGreedy(bb_env)
    tq_algo_lin = TabularQLearning(bb_env, num_episodes=num_episodes, exploration_policy=lin_expp)

    # LIN Q(lambda)
    tql_algo_lin = TabularQLambda(bb_env, num_episodes=num_episodes) # TODO optimize!

    # ELA
    ELA_scalarization = WeightedScalarization(utility=vELA)
    ELA_order_op = ScalarizingOrderOperator(ELA_scalarization)
    #ELA_expp = EpsilonGreedy(bb_env, order_operation=ELA_order_op)
    ELA_expp = SoftmaxExploration(bb_env, order_operation=ELA_order_op, ranking_method="tournament")
    tq_algo_ela = TabularQLearning(bb_env, num_episodes=num_episodes, exploration_policy=ELA_expp)

    # SFLLA
    SFLLA_scalarization = WeightedScalarization(utility=vSFLLA)
    SFLLA_order_op = ScalarizingOrderOperator(SFLLA_scalarization)
    SFLLA_expp = EpsilonGreedy(bb_env, epsilon=0.1, decay_params={"decay_rate": 0.9999}, order_operation=SFLLA_order_op)
    tq_algo_sflla = TabularQLearning(bb_env, alpha=0.01, num_episodes=num_episodes, exploration_policy=SFLLA_expp)

    # LO
    LO_op = LexicographicOrderOperator(objective_order=[2, 1, 0])
    LO_expp = EpsilonGreedy(bb_env, epsilon=0.1, decay_params={"decay_rate": 0.9999}, order_operation=LO_op)
    tq_algo_lo = TabularQLearning(bb_env, alpha=0.01, num_episodes=num_episodes, exploration_policy=LO_expp)

    algo_dict = {
            #"Linear, tabular Q": tq_algo_lin,
            #"ELA, tabular Q": tq_algo_ela,
            #"SFLLA, tabular Q": tq_algo_sflla,
            #"Linear, tabular Q(Lambda)": tql_algo_lin,
            "LO, tabular Q": tq_algo_lo
            }

    plt.figure()
    for (label, algo) in algo_dict.items():
        t_start = time.time()
        algo.learn()
        print(f"Finished {label} in {time.time() - t_start:.2f}s")
        plt.plot(algo.returns[0, :], label=label)
    plt.xlabel("episode")
    plt.ylabel("return")
    plt.grid()
    plt.legend()
    plt.show()