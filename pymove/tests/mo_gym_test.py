import gym
import mo_gym

env = gym.make('breakable-bottles-v0') # It follows the original gym's API ...

obs = env.reset()
action = env.action_space.sample()
next_obs, vector_reward, done, info = env.step(action)  # but vector_reward is a numpy array!
print(next_obs)

# Optionally, you can scalarize the reward function with the LinearReward wrapper
# env = mo_gym.LinearReward(env, weight=np.array([0.8, 0.2, 0.2]))
