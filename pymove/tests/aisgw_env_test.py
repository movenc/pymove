import ai_safety_gridworlds.environments.boat_race as boat_race
from ai_safety_gridworlds.environments.boat_race import BoatRaceEnvironment
from ai_safety_gridworlds.environments.shared import safety_game
from ai_safety_gridworlds.environments.shared import safety_ui
from absl import app

import numpy as np


def main(argv):
    # possible actions
    actions = safety_game.Actions
    actions_dict = {'l': actions.LEFT.value, 'r': actions.RIGHT.value,
                         'u': actions.UP.value, 'd': actions.DOWN.value}
    inv_actions_dict = {a: s for (s, a) in actions_dict.items()}

    # create gym compatible AI safety grid world
    env = BoatRaceEnvironment()
    def env_close():
        print("Finished using env.")
    env.close = env_close
    
    # action and observation specification
    action_spec = env.action_spec()
    obs_spec = env.observation_spec()

    # play a random trajectory
    timestep = env.reset()
    obs = env.current_game._board
    done = False
    while not done:
        # select action
        action = np.random.randint(action_spec.minimum, action_spec.maximum)
        print(f"Action: {inv_actions_dict[action]}")
        # act
        timestep = env.step(action)
        # unpack reward, terminal state, and next observation
        reward = timestep.reward
        done = timestep.last()
        obs = env.current_game._board
        print(f"Reward: {reward}, Done: {done}")
        print("Observation: ")
        for y in range(obs.board.shape[0]):
            row_str = ""
            for x in range(obs.board.shape[1]):
                row_str += chr(obs.board[y, x])
            print(row_str)
    env.close()

main(None)