import safe_grid_gym
import gym

env = gym.make("SideEffectsSokoban-v0")
action_space = env.action_space
env.reset()
for i in range(10):
   action = action_space.sample()
   state, reward, done, info = env.step(action)
   env.render(mode="human")