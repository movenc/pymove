import numpy as np
from pymove.lex import lexicographic_order, thresholded_lexicographic_order

class OrderOperator:
    def __init__(self):
        pass

    def argmax(self, vecs):
        """ Returns index of maximum utility vector """
        pass

    def argsort(self, vecs):
        """ Returns index list ordered according the operator """
        pass

    def order(self, vec1, vec2):
        """ Returns 1 if vec1 > vec2, -1 if vec2 > vec1 and 0 if vec1 == vec2 """
        pass
    
class ScalarizingOrderOperator(OrderOperator):
    def __init__(self, scalarization=None):
        if scalarization is None:
            self.scalarization = lambda x: x.mean(axis=0)
        else:
            self.scalarization = scalarization
    
    def argmax(self, vecs):
        scalars = None
        if isinstance(vecs, list):
            scalars = np.array([self.scalarization(vec) for vec in vecs])
        elif isinstance(vecs, np.ndarray):
            scalars = self.scalarization(vecs)
        argmax_scalars = np.flatnonzero(scalars == scalars.max())
        return argmax_scalars
 
    def argsort(self, vecs):
        # Note that argsort performs "ordinal" ranking
        permutation = None
        if isinstance(vecs, list):
            permutation = np.argsort([self.scalarization(vec) for vec in vecs])
            return permutation
        elif isinstance(vecs, np.ndarray):
            permutation = np.argsort(self.scalarization(vecs))
            return permutation

    def order(self, vec1, vec2):
        sc1 = self.scalarization(vec1)
        sc2 = self.scalarization(vec2)
        if sc1 > sc2: return 1
        elif sc2 > sc1: return -1
        else: return 0


class LexicographicOrderOperator(OrderOperator):
    def __init__(self, objective_order=None, thresholds=None):
        # assumes that thresholds vector is already in the correct order
        if objective_order is not None:
            # verify that objective_order is a permutation
            for i in range(len(objective_order)):
                assert i in objective_order
            # check that appropriate number of thresholds is provided
            if thresholds is not None:
                assert len(objective_order) == len(thresholds) - 1

        self.objective_order = objective_order
        self.thresholds = thresholds

    def argmax(self, vecs):
        best_as = [0]
        best_vec_instance = vecs[0]
        for a, vec in enumerate(vecs):
            if self.order(vec, best_vec_instance) == 1:
                best_as.clear()
                best_as.append(a)
                best_vec_instance = vec
            elif self.order(vec, best_vec_instance) == 0:
                best_as.append(a)
            # else: this vec is worse than best one
        return np.array(best_as)

    def order(self, vec1, vec2):
        if self.objective_order is not None:
            vec1 = vec1[self.objective_order]
            vec2 = vec2[self.objective_order]   
        if self.thresholds is not None:
            return thresholded_lexicographic_order(vec1, vec2, thresholds=self.thresholds)
        else:
            return lexicographic_order(vec1, vec2)


 