import numpy as np
from gym.spaces import flatdim
from pymove.exploration import EpsilonGreedy

class QLearning:
    def __init__(self, env,
                num_episodes=1,
                eps_convergence=1e4,
                alpha=0.1,
                alpha_decay=1.,
                gamma=1.,
                exploration_policy=None,
                feature_map=None,
                use_replay=False,
                replays_per_step=10,
                add_past_cum_reward=False,
                decay_learning_rate=None,
                decay_exploration_rate=None,
                weight_init=0.):
        self.env = env
        self.num_episodes = num_episodes
        self.eps_convergence = eps_convergence
        self.initial_alpha = alpha
        self.alpha_decay = alpha_decay
        self.gamma = gamma
        self.feature_map = feature_map
        self.use_replay = use_replay
        self.replays_per_step = replays_per_step
        self.add_past_cum_reward = add_past_cum_reward
        self.weight_init = weight_init
        if decay_learning_rate is None:
            self.decay_learning_rate = lambda step, done: done
        else:
            self.decay_learning_rate = decay_learning_rate
        if decay_exploration_rate is None:
            self.decay_exploration_rate = lambda step, done: done
        else:
            self.decay_exploration_rate = decay_exploration_rate

        if exploration_policy is None:
            self.exploration_policy = EpsilonGreedy(self.env)
        else:
            self.exploration_policy = exploration_policy

        # initialize Q function approximator weights (one for each objective)
        self.q_weights = np.random.uniform(-self.weight_init, self.weight_init, (self.feature_map.size, self.env.num_objectives))
        #self.q_weights = np.zeros((self.feature_map.size, self.env.num_objectives))
        self.returns = np.zeros((self.env.num_objectives, self.num_episodes))
        self.past_cum_reward = np.zeros(self.env.num_objectives)

    def learn(self, return_trajectories=False, verbose=False):
        experience_buffer = []
        converged = False
        trajectories = [[] for _ in range(self.num_episodes)]
        total_steps = 0
        # learn for num_episodes
        for eps in range(self.num_episodes):
            if converged:
                break
            if verbose:
                print(f"Episode {eps} | epsilon {self.exploration_policy.epsilon}")
            # initialize episode
            alpha = self.initial_alpha
            done = False
            obs = self.env.reset()
            step_counter = 0
            self.past_cum_reward = np.zeros(self.env.num_objectives)
            #total_weight_update_norm = 0.
            while not done:
                # select action
                features = np.array([self.feature_map(obs, a) for a in range(self.env.num_actions)])
                q_values = (features @ self.q_weights).T # shape (num_objectives, num_actions)
                if self.add_past_cum_reward:
                    #q_values_with_past = self.past_cum_reward[:, None] + q_values
                    q_values_with_past = self.env.past_return[:, None] + q_values
                    action = self.exploration_policy.select(q_values_with_past)
                else:
                    action = self.exploration_policy.select(q_values)

                #print(f"Action {action}")
                # take action
                emission = self.env.step(action)
                total_steps += 1
                step_counter += 1
                if return_trajectories:
                    trajectories[eps].append(emission)

                obs_prime, reward, done, info = emission

                # store transition in experience buffer
                if not self.use_replay:
                    experience_buffer = []
                experience_buffer.append((obs, action, reward, obs_prime))

                # keep track of return and past cumulative rewards
                self.returns[:, eps] += self.gamma**eps*np.array(reward)
                self.past_cum_reward[:] = self.gamma*self.past_cum_reward[:] + np.array(reward)

                if not done: # do not maintain value for terminal states
                    if self.use_replay and len(experience_buffer) >= self.replays_per_step + 1:
                        # sample past experience to be trained on
                        random_sample_idxs = np.random.choice(len(experience_buffer) - 1, self.replays_per_step, replace=False).tolist()
                        random_sample_idxs.append(len(experience_buffer) - 1)
                    else:
                        random_sample_idxs = [len(experience_buffer) - 1]

                    for si, sample_idx in enumerate(random_sample_idxs):
                        obs_upd, a_upd, r_upd, obs_upd_prime = experience_buffer[sample_idx]
                        # calculate Q(s, a)
                        features_upd = self.feature_map(obs_upd, a_upd) # shape = (num_features, )
                        q_values_upd = (features_upd @ self.q_weights).T # shape = (num_objectives, )

                        # calculate Q(s', a*)
                        features_upd_prime = np.array([self.feature_map(obs_upd_prime, a) for a in range(self.env.num_actions)])
                        q_values_upd_prime = (features_upd_prime @ self.q_weights).T # shape = (num_objectives, num_actions)
                        if self.add_past_cum_reward:
                            a_upd_prime_greedy = self.exploration_policy.select(self.env.past_return[:, None] + q_values_upd_prime, greedy=True)
                        else:
                            a_upd_prime_greedy = self.exploration_policy.select(q_values_upd_prime, greedy=True)

                        # calculate TD error
                        delta = r_upd + self.gamma*q_values_upd_prime[:, a_upd_prime_greedy] - q_values_upd # shape = (num_objectives, )

                        # update state-action values
                        delta = np.expand_dims(delta, 0)
                        features_upd = np.expand_dims(features_upd, 1)
                        weight_update = alpha*delta*features_upd
                        self.q_weights[:, :] += weight_update
                        #total_weight_update_norm += np.linalg.norm(weight_update)

                    obs = obs_prime

                # update learning rate
                if self.decay_learning_rate(total_steps, done):
                    alpha *= self.alpha_decay
                # update exploration rate
                if self.decay_exploration_rate(total_steps, done):
                    print(f"Episode {eps}, Step {step_counter}, Epsilon {self.exploration_policy.epsilon:0.5f}")
                    self.exploration_policy.decay()
                if verbose:
                    print(f"Episode {eps}, Step {step_counter}, Action {action}, Reward: {reward}")#, Update norm: {total_weight_update_norm}")
            # stop learning if the total update norm is smaller than the convergence threshold
            #if total_weight_update_norm <= self.eps_convergence:
            #    converged = False

        if return_trajectories:
            return trajectories


if __name__ == "__main__":
    # test Q-learning with function approximation
    import matplotlib.pyplot as plt
    import gym
    import mo_gym

    from pymove.scalarizations import WeightedScalarization, ELA, SFLLA, vELA, vSFLLA
    from pymove.order_operators import ScalarizingOrderOperator, LexicographicOrderOperator
    from pymove.exploration import SoftmaxExploration
    from pymove.approximation import TileCode
    from pymove.env_wrappers import PastReturnAugmented, ObjectiveSelector

    import time
    
    # environment
    num_steps = 2000
    gamma = 0.9
    gamma_past = 0.99
    #env = gym.make("food-water-balance-v0", max_steps=num_steps, gamma=gamma, gamma_past=gamma_past)
    env = PastReturnAugmented(ObjectiveSelector(gym.make("breakable-bottles-v0"), [1, 2]), gamma_past)

    cum_rewards_range = env.observation_space["past_return"].high - env.observation_space["past_return"].low

    # algorithm parameters
    num_episodes = 100
    alpha = 0.05
    alpha_decay = 1.0
    epsilon = 0.5
    epsilon_decay = 0.9995
    epsilon_update_freq = 2000
    use_replay = False
    num_random_replays = 10

    # setup features map
    iht_size = 4096
    num_tilings = 8
    tile_size = 1/num_tilings

    def state_action_parser(s, a):
        ds_dict, w = s
        ds = env.unwrapped.get_obs_idx(ds_dict)
        dsa = ds + a*env.num_observations
        return w, np.array(dsa)

    float_dim_bounds = np.array([env.observation_space["past_return"].low, env.observation_space["past_return"].high])
    int_dim_bounds = np.array([[0], [5]])
    tile_code = TileCode(num_tilings, float_dim_bounds, int_dim_bounds, state_action_parser)

    # setup exploration policy
    utility_function = lambda x: x
    #utility_function = lambda x: -np.exp(-x)
    #utility_function = lambda x: np.where(x < 0, 1 - np.exp(-x), np.log(1 + x*(x >= 0)))

    scalarization = WeightedScalarization(utility=vSFLLA)
    order_op = ScalarizingOrderOperator(scalarization)
    exploration_policy = EpsilonGreedy(env, epsilon=epsilon, decay_params={"decay_rate": epsilon_decay}, order_operation=order_op)
    
    algo = QLearning(env,
                num_episodes,
                alpha,
                alpha_decay=alpha_decay,
                gamma=gamma,
                exploration_policy=exploration_policy,
                feature_map=tile_code,
                use_replay=use_replay,
                replays_per_step=num_random_replays)

    algo.learn()

    plt.figure()
    plt.plot(algo.returns[0, :], label="food")
    plt.plot(algo.returns[1, :], label="water")
    plt.legend()
    plt.xlabel("steps")
    plt.ylabel("return")
    plt.grid()
    plt.show()