import numpy as np
import numba

@numba.jit(nopython=True)
def lexicographic_order(vec1, vec2):
    n = len(vec1)
    for i in range(n):
        if vec1[i] > vec2[i]:
            return 1
        else:
            if vec2[i] > vec2[1]:
                return -1
    return 0


@numba.jit(nopython=True)
def thresholded_lexicographic_order(vec1, vec2, thresholds):
    n = len(vec1)
    assert len(thresholds) == n - 1
    T_a = np.zeros_like(thresholds)
    T_b = np.zeros_like(thresholds)
    for i in range(n - 1):
        T_a[i] = min(vec1[i], thresholds[i])
        T_b[i] = min(vec2[i], thresholds[i])
    lex = lexicographic_order(T_a, T_b)
    if lex != 0:
        return lex
    else:
        return lexicographic_order(vec1, vec2)
