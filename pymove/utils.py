import numpy as np

def gini_coefficient(utilities):
    num_samples, num_parties = utilities.shape
    denom = np.sum([np.abs(utilities[:, i] - utilities[:, j]) for i in range(num_parties) for j in range(num_parties)], axis=0)
    nom = 2*num_parties*np.abs(np.sum(utilities, axis=1))
    return denom/nom

def get_avg_stay_lengths(transition_buffer, num_observations):
    """ calculate the average state visit lengths """
    size_buffer = len(transition_buffer)
    avg_stay_lengths = np.zeros((size_buffer, num_observations))
    for si in range(num_observations):
        stay_steps = []
        stay_lengths = []
        stay_counter = 0
        for st in range(size_buffer):
            s, s_prime = transition_buffer[st]
            if s == si:
                stay_counter += 1
            if s == si and s != s_prime:
                stay_steps.append(st)
                stay_lengths.append(stay_counter)
                stay_counter = 0
        stay_steps = np.array(stay_steps)
        stay_lengths = np.array(stay_lengths)
        for st in range(size_buffer):
            avg_stay_lengths[st, si] = np.mean(stay_lengths[stay_steps <= st])
    return avg_stay_lengths

def numpy_ewma_vectorized(data, window):
    """ calculates the exponentially weighted moving average """
    alpha = 2 /(window + 1.0)
    alpha_rev = 1-alpha

    scale = 1/alpha_rev
    n = data.shape[0]

    r = np.arange(n)
    scale_arr = scale**r
    offset = data[0, :, None]*alpha_rev**(r+1)[None, :]
    pw0 = alpha*alpha_rev**(n-1)

    mult = scale_arr[:, None]*data*pw0
    cumsums = mult.cumsum(axis=0)
    out = offset.T + scale_arr[::-1][:, None]*cumsums
    return out

if __name__ == "__main__":
    x = np.random.random((20, 4))
    print(gini_coefficient(x))