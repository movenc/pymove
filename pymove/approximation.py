import numpy as np
from pymove.tiles3 import *

class FeatureMap:
    def __call__(self, float_dims, int_dims):
        raise NotImplementedError("A feature map needs to be callable.")


class TileCode(FeatureMap):
    def __init__(self, num_tilings, float_dim_bounds, int_dim_bounds, state_action_parser, iht_size=None):
        self.state_action_parser = state_action_parser
        self.num_tilings = num_tilings
        self.tile_size = 1/self.num_tilings
        self.num_float_dims = len(float_dim_bounds[0])
        self.float_dim_bounds = float_dim_bounds
        self.float_dim_ranges = self.float_dim_bounds[1] - self.float_dim_bounds[0]
        self.num_int_dims = len(int_dim_bounds[0])
        self.int_dim_bounds = int_dim_bounds
        self.int_dim_ranges = self.int_dim_bounds[1] - self.int_dim_bounds[0]
        self.total_int_dim_size = np.prod(self.int_dim_ranges)
        self.size = iht_size
        if self.size is None:
            self.size = 2**(int(np.log2(self.num_tilings**(self.num_float_dims + 1)*self.total_int_dim_size)) + 1)
        self.iht = IHT(self.size)


    def __call__(self, state, action):
        float_dims, int_dims = self.state_action_parser(state, action)
        #if not isinstance(int_dims, list) or not isinstance(int_dims, np.ndarray):
        #    int_dims = np.array([int_dims])
        indices = tiles(self.iht, self.num_tilings, (float_dims/self.tile_size/self.float_dim_ranges), int_dims.tolist())
        feature_vector = np.zeros(self.size)
        feature_vector[indices] = 1
        return feature_vector


