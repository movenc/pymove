import numpy as np
from pymove.order_operators import ScalarizingOrderOperator

class ExplorationPolicy:
    def __init__(self, env, order_operation=ScalarizingOrderOperator()):
        self.env = env
        self.order_operation = order_operation

    def select_greedy(self, q_values):
        greedy_actions = self.order_operation.argmax(q_values)
        action = np.random.choice(greedy_actions)
        return action

class EpsilonGreedy(ExplorationPolicy):
    decay_types = ["exp", "lin"]
    default_decay_params = [{"decay_rate": 0.99}, {"decay_step": 0.01}]

    def __init__(self, env,
                epsilon=0.05,
                decay_type=decay_types[0],
                decay_params=default_decay_params[0], 
                order_operation=ScalarizingOrderOperator()):
        assert epsilon >= 0 and epsilon < 1
        assert decay_type in self.decay_types
        self.epsilon = epsilon
        self.initial_epsilon = epsilon
        self.decay_type = decay_type
        self.decay_params = decay_params
        self.order_operation = order_operation

        super().__init__(env, order_operation)

    def select(self, q_values, greedy=False):
        # Q-values are for a given state Q(s,.)

        # select according to epsilon greedy
        if not greedy and np.random.random() < self.epsilon:
            action = np.random.randint(self.env.num_actions)        
        else:
            action = self.select_greedy(q_values)
        return action

    def decay(self):
        # decay epsilon
        if self.decay_type == "exp":
            self.epsilon *= self.decay_params["decay_rate"]
        elif self.decay_type == "lin":
            self.epsilon = max(self.epsilon - self.decay_params["decay_step"], 0)


class SoftmaxExploration(ExplorationPolicy):
    decay_types = ["exp", "lin"]
    default_decay_params = [{"decay_rate": 1.}, {"decay_step": 0.01}]
    ranking_methods = ["tournament", "epsilon"]

    # starting tau 10
    # tau_decay Math.pow(0.01/startingTemperature,1.0/numEpisodes)
    def __init__(self, env,
                tau=10.,
                decay_type=decay_types[0],
                decay_params=default_decay_params[0], 
                order_operation=ScalarizingOrderOperator(),
                ranking_method=ranking_methods[0]):
        assert tau >= 0
        assert decay_type in self.decay_types
        self.tau = tau 
        self.initial_tau = tau
        self.decay_type = decay_type
        self.decay_params = decay_params
        self.order_operation = order_operation
        self.ranking_method = ranking_method

        super().__init__(env, order_operation)

    @property
    def epsilon(self):
        return self.tau

    def select(self, q_values):
        # Q-values are for a given state Q(s,.)
        # compute scores
        if isinstance(self.order_operation, ScalarizingOrderOperator):
            scores = self.order_operation.scalarization(q_values)
        elif isinstance(self.order_operation, LexicographicOrderOperator):
            if self.ranking_method == "tournament":
                scores = np.zeros(q_values.shape[1])
                for a in range(q_values.shape[1]):
                    for b in set(range(q_values.shape[1])) - {a}:
                        if self.order_operation.order(q_values[:, a], q_values[:, b]) >= 0:
                            scores[a] += 1
            elif self.ranking_method == "epsilon":
                scores = np.zeros(q_values.shape[1])
                min_obj = q_values.min(axis=1)
                max_obj = q_values.max(axis=1)
                max_min_obj = max_obj - min_obj
                a_greedy = self.order_operation.argmax(q_values)
                for a in range(q_values.shape[1]):
                    for o in range(q_values.shape[0]):
                        u_eps = (q_values[o, a_greedy] - q_values[o, a])/max_min_obj[o]
                        if u_eps > scores[a]:
                            scores[a] = u_eps
                    scores[a] = 1 - scores[a]

        # select according to softmax distribution
        p_a = np.exp(scores/self.tau)/np.sum(np.exp(scores/self.tau))
        action = np.random.choice(self.env.num_actions, p=p_a)       

        return action

    def decay(self):
        # decay epsilon
        if self.decay_type == "exp":
            self.tau *= self.decay_params["decay_rate"]
        elif self.decay_type == "lin":
            self.tau = max(self.tau - self.decay_params["decay_step"], 0)
