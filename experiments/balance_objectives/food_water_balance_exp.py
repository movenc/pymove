import numpy as np
import matplotlib.pyplot as plt

import gym
import mo_gym

from pymove.approx_q_learning import QLearning
from pymove.exploration import EpsilonGreedy
from pymove.scalarizations import WeightedScalarization
from pymove.order_operators import ScalarizingOrderOperator
from pymove.approximation import TileCode
from pymove.env_wrappers import PastReturnAugmented, ObjectiveSelector
from pymove.utils import *


#from line_profiler import LineProfiler

# environment
num_steps = 1000
gamma = 0.9
gamma_past = 0.99
env = PastReturnAugmented(gym.make("food-water-balance-v0", max_steps=num_steps), gamma_past)
cum_rewards_range = env.observation_space["past_return"].high - env.observation_space["past_return"].low

# algorithm parameters
verbose = True
num_episodes = 1
alpha = 0.2
alpha_decay = 1.0
epsilon = 0.5
epsilon_decay = 0.5
epsilon_update_freq = 2000
use_replay = True
num_random_replays = 10
add_past_cum_reward = True
decay_exploration_rate = lambda step, done: (step + 1) % epsilon_update_freq == 0
weight_init = 0.01

# setup features map
iht_size = 4096
num_tilings = 14
tile_size = 1/num_tilings

def state_action_parser(s, a):
    ds, w = s
    dsa = ds + a*env.num_observations
    return w, np.array([dsa])

float_dim_bounds = np.array([env.observation_space["past_return"].low, env.observation_space["past_return"].high])
int_dim_bounds = np.array([[0, 0], [env.action_space.n, env.observation_space["obs"].n]])
tile_code = TileCode(num_tilings, float_dim_bounds, int_dim_bounds, state_action_parser)

# setup behavior policy
#utility_function = lambda x: x # linear
#utility_function = lambda x: -np.exp(-x) # MORE
utility_function = lambda x: np.where(x < 0, 1 - np.exp(-x), np.log(1 + x*(x >= 0))) # explog
scalarization = WeightedScalarization(utility=utility_function)
order_op = ScalarizingOrderOperator(scalarization)
exploration_policy = EpsilonGreedy(env, epsilon=epsilon, decay_type="exp", decay_params={"decay_rate": epsilon_decay}, order_operation=order_op)

# run the learning algorithm
algo = QLearning(env,
            num_episodes,
            alpha,
            alpha_decay=alpha_decay,
            gamma=gamma,
            exploration_policy=exploration_policy,
            feature_map=tile_code,
            use_replay=use_replay,
            replays_per_step=num_random_replays,
            decay_exploration_rate=decay_exploration_rate,
            add_past_cum_reward=add_past_cum_reward,
            weight_init=weight_init)

#lp = LineProfiler()
#lp_wrapper = lp(algo.learn)
#trajectories = lp_wrapper(verbose=verbose, return_trajectories=True)
trajectories = algo.learn(verbose=verbose, return_trajectories=True)

# process trajectories
traj = trajectories[0]
past_returns_per_step = np.array([traj[i][0][1] for i in range(len(traj))])

window_steps = 1000
ewma_past_returns = numpy_ewma_vectorized(past_returns_per_step, window_steps)

transition_buffer = [(traj[i-1][0][0], traj[i][0][0]) for i in range(1, num_steps)]
avg_stay_lengths = get_avg_stay_lengths(transition_buffer, env.num_observations)

rewards_over_time = np.array([traj[i][1] for i in range(len(traj))])
cum_rewards_over_time = np.vstack((np.cumsum(rewards_over_time[:, 0]), np.cumsum(rewards_over_time[:, 1])))
nonlin_value = scalarization(cum_rewards_over_time)

# plot some results
fig, axes = plt.subplots(4, 1, sharex=True)
axes[0].plot(past_returns_per_step[:, 0], label="food")
axes[0].plot(past_returns_per_step[:, 1], label="water")
axes[0].legend()
axes[0].set_ylabel("past return")
axes[0].grid()
axes[1].plot(ewma_past_returns[:, 0], label="food")
axes[1].plot(ewma_past_returns[:, 1], label="water")
axes[1].legend()
axes[1].set_ylabel("ewma past return")
axes[1].grid()
for st in range(env.num_observations):
    axes[2].plot(avg_stay_lengths[:, st], label=f"state {st}")
axes[2].set_ylabel("avg. stay length")
axes[2].grid()
axes[2].legend()
axes[3].plot(nonlin_value, label="nonlinear value")
axes[3].grid()
axes[3].legend()
axes[-1].set_xlabel("steps")

plt.figure()
plt.plot(np.concatenate((np.zeros(50), gini_coefficient(past_returns_per_step)[50:])))
plt.xlabel("step")
plt.ylabel("gini coefficient")
plt.show()

lp.print_stats()