import numpy as np
import matplotlib.pyplot as plt

import gym
from gym.spaces import Box, Discrete
import safe_grid_gym

from pymove.approx_q_learning import QLearning
from pymove.exploration import EpsilonGreedy
from pymove.scalarizations import WeightedScalarization
from pymove.order_operators import ScalarizingOrderOperator
from pymove.approximation import TileCode
from pymove.env_wrappers import PastReturnAugmented, ObjectiveSelector


# environment
num_steps = 1000
level = 2 # which level of the grid world to use
gamma = 0.9 # future discount factor (the lower the value the more myopic the agent)
gamma_past = 0.99 # past discount factor defines the "memory capacity" of the agent
bare_env = gym.make("FoodDrinkUnbounded-v0")
# Monkey patch reward and action space
bare_env.reward_space = Box(np.array([0, 0]), np.array([1, 1]))
bare_env.action_space = Discrete(5) # 4 dirs + noop
# select only movement penalty, food and water as objectives
env = PastReturnAugmented(ObjectiveSelector(bare_env, [0, 1]), gamma_past)
# Monkey patch some assumed properties to the environment
obs_dims = np.array(list(env.observation_space["obs"].shape[1:])) - 2
def get_obs_idx(obs):
    agent_pos = np.where(obs[0,:,:] == 2.)
    multi_index = np.array([[agent_pos[0][0] - 1], [agent_pos[1][0] - 1]])
    return np.ravel_multi_index(multi_index, obs_dims)
env.get_obs_idx = get_obs_idx
env.num_actions = bare_env.action_space.n
env.num_observations = np.prod(obs_dims)


# algorithm parameters
verbose = True
num_episodes = 1
alpha = 0.1 # learning rate
alpha_decay = 1.0
epsilon = 0.5 # exploration rate
epsilon_decay = 0.5
epsilon_update_freq = 100
decay_exploration_rate = lambda step, done: (step + 1) % epsilon_update_freq == 0
use_replay = True
num_random_replays = 10
add_past_cum_reward = True


# setup features map
iht_size = 4096
num_tilings = 8
tile_size = 1/num_tilings

def state_action_parser(s, a):
    ds_dict, w = s
    ds = env.get_obs_idx(ds_dict)
    dsa = ds + a*np.prod(env.observation_space["obs"].shape)
    return w, np.array(dsa)

float_dim_bounds = np.array([env.observation_space["past_return"].low, env.observation_space["past_return"].high])
int_dim_bounds = np.array([[[0], [sp]] for sp in obs_dims]).squeeze(axis=-1).T
tile_code = TileCode(num_tilings, float_dim_bounds, int_dim_bounds, state_action_parser)


# setup behavior policy
#utility_function = lambda x: x # linear
utility_function = lambda x: -np.exp(-x) # MORE
#utility_function = lambda x: np.where(x < 0, 1 - np.exp(-x), np.log(1 + x*(x >= 0))) # explog
scalarization = WeightedScalarization(utility=utility_function)
order_op = ScalarizingOrderOperator(scalarization)
exploration_policy = EpsilonGreedy(env, epsilon=epsilon, decay_type="exp", decay_params={"decay_rate": epsilon_decay}, order_operation=order_op)


# run the learning algorithm
algo = QLearning(env,
            num_episodes,
            alpha,
            alpha_decay=alpha_decay,
            gamma=gamma,
            exploration_policy=exploration_policy,
            feature_map=tile_code,
            use_replay=use_replay,
            replays_per_step=num_random_replays,
            decay_exploration_rate=decay_exploration_rate,
            add_past_cum_reward=add_past_cum_reward)

trajectories = algo.learn(verbose=verbose, return_trajectories=True)


# process trajectories
traj = trajectories[0]
past_returns_per_step = np.array([traj[i][0][1] for i in range(len(traj))])

def numpy_ewma_vectorized(data, window):
    """ calculates the exponentially weighted moving average """
    alpha = 2 /(window + 1.0)
    alpha_rev = 1-alpha

    scale = 1/alpha_rev
    n = data.shape[0]

    r = np.arange(n)
    scale_arr = scale**r
    offset = data[0, :, None]*alpha_rev**(r+1)[None, :]
    pw0 = alpha*alpha_rev**(n-1)

    mult = scale_arr[:, None]*data*pw0
    cumsums = mult.cumsum(axis=0)
    out = offset.T + scale_arr[::-1][:, None]*cumsums
    return out

window_steps = 100
ewma_past_returns = numpy_ewma_vectorized(past_returns_per_step, window_steps)

def get_avg_stay_lengths(transition_buffer):
    """ calculate the average state visit lengths """
    size_buffer = len(transition_buffer)
    avg_stay_lengths = np.zeros((size_buffer, env.num_observations))
    for si in range(env.num_observations):
        stay_steps = []
        stay_lengths = []
        stay_counter = 0
        for st in range(size_buffer):
            s, s_prime = transition_buffer[st]
            s = env.get_obs_idx(s)
            s_prime = env.get_obs_idx(s_prime)
            if s == si and s == s_prime:
                stay_counter += 1
            if s == si and s != s_prime:
                stay_steps.append(st)
                stay_lengths.append(stay_counter)
                stay_counter = 0
        stay_steps = np.array(stay_steps)
        stay_lengths = np.array(stay_lengths)
        for st in range(size_buffer):
            avg_stay_lengths[st, si] = np.mean(stay_lengths[stay_steps <= st])
    return avg_stay_lengths

transition_buffer = [(traj[i-1][0][0], traj[i][0][0]) for i in range(1, num_steps)]
avg_stay_lengths = get_avg_stay_lengths(transition_buffer)

# plot some results
fig, axes = plt.subplots(3, 1, sharex=True)
axes[0].plot(past_returns_per_step[:, 0], label="water")
axes[0].plot(past_returns_per_step[:, 1], label="food")
axes[0].legend()
axes[0].set_ylabel("past return")
axes[0].grid()
axes[1].plot(ewma_past_returns[:, 0], label="water")
axes[1].plot(ewma_past_returns[:, 1], label="food")
axes[1].legend()
axes[1].set_ylabel("ewma past return")
axes[1].grid()
st_names = ["water", "gap", "food"]
for st in range(env.num_observations):
    if st in [0, 1, 2]:
        axes[2].plot(avg_stay_lengths[:, st], label=f"{st_names[st]} tile")
axes[2].set_ylabel("avg. stay length")
axes[2].grid()
axes[2].legend()
axes[-1].set_xlabel("steps")
plt.show()

# plot some results
"""
fig, axs = plt.subplots(3, sharex=True)
axs[0].plot(algo.returns[0, :], label="movement penalty")
axs[1].plot(algo.returns[1, :], label="food")
axs[2].plot(algo.returns[2, :], label="water")
axs[2].set_xlabel("steps")
axs[1].set_ylabel("return")
axs[0].grid()
axs[1].grid()
axs[2].grid()
axs[0].legend()
axs[1].legend()
axs[2].legend()
plt.show()
"""