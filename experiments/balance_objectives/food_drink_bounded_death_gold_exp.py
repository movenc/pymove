import numpy as np
import matplotlib.pyplot as plt

import gym
from gym.spaces import Box, Discrete
import safe_grid_gym

from pymove.approx_q_learning import QLearning
from pymove.exploration import EpsilonGreedy
from pymove.scalarizations import WeightedScalarization
from pymove.order_operators import ScalarizingOrderOperator
from pymove.approximation import TileCode
from pymove.env_wrappers import PastReturnAugmented, ObjectiveSelector


# environment
num_steps = 100
level = 2 # which level of the grid world to use
gamma = 0.9 # future discount factor (the lower the value the more myopic the agent)
gamma_past = 0.99 # past discount factor defines the "memory capacity" of the agent
bare_env = gym.make("FoodDrinkBoundedDeathGold-v0", max_iterations=num_steps)
# Monkey patch reward and action space
bare_env.reward_space = Box(np.array([-1, -1, -50, 0]), np.array([0, 0, 0, 40]))
bare_env.action_space = Discrete(5) # 4 dirs + noop
# select only movement penalty, food and water as objectives
objective_names = ["drink deficiency", "food deficiency", "death", "gold"]
selected_objectives = [0, 1, 2, 3]
selected_objective_names = [objective_names[ob] for ob in selected_objectives]
env = PastReturnAugmented(ObjectiveSelector(bare_env, selected_objectives), gamma_past)
# Monkey patch some assumed properties to the environment
obs_dims = np.array(list(env.observation_space["obs"].shape[1:])) - 2
def get_obs_idx(obs):
    agent_pos = np.where(obs[0,:,:] == 2.)
    multi_index = np.array([[agent_pos[0][0] - 1], [agent_pos[1][0] - 1]])
    return np.ravel_multi_index(multi_index, obs_dims)
env.get_obs_idx = get_obs_idx
env.num_actions = bare_env.action_space.n
env.num_observations = np.prod(obs_dims)


# algorithm parameters
verbose = True
num_episodes = 20
alpha = 0.1 # learning rate
alpha_decay = 1.0
epsilon = 0.5 # exploration rate
epsilon_decay = 0.9
epsilon_update_freq = 100
decay_exploration_rate = None#lambda step, done: (step + 1) % epsilon_update_freq == 0
use_replay = True
num_random_replays = 10
add_past_cum_reward = True


# setup features map
iht_size = 4096
num_tilings = 8
tile_size = 1/num_tilings

def state_action_parser(s, a):
    ds_dict, w = s
    ds = env.get_obs_idx(ds_dict)
    dsa = ds + a*np.prod(env.observation_space["obs"].shape)
    return w, np.array(dsa)

float_dim_bounds = np.array([env.observation_space["past_return"].low, env.observation_space["past_return"].high])
int_dim_bounds = np.array([[[0], [sp]] for sp in obs_dims]).squeeze(axis=-1).T
tile_code = TileCode(num_tilings, float_dim_bounds, int_dim_bounds, state_action_parser)


# setup behavior policy
#utility_function = lambda x: x # linear
utility_function = lambda x: -np.exp(-x) # MORE
#utility_function = lambda x: np.where(x < 0, 1 - np.exp(-x), np.log(1 + x*(x >= 0))) # explog
scalarization = WeightedScalarization(utility=utility_function)
order_op = ScalarizingOrderOperator(scalarization)
exploration_policy = EpsilonGreedy(env, epsilon=epsilon, decay_type="exp", decay_params={"decay_rate": epsilon_decay}, order_operation=order_op)


# run the learning algorithm
algo = QLearning(env,
            num_episodes,
            alpha,
            alpha_decay=alpha_decay,
            gamma=gamma,
            exploration_policy=exploration_policy,
            feature_map=tile_code,
            use_replay=use_replay,
            replays_per_step=num_random_replays,
            decay_exploration_rate=decay_exploration_rate,
            add_past_cum_reward=add_past_cum_reward)

trajectories = algo.learn(verbose=verbose, return_trajectories=True)

# plot some results
plt.figure()
for obj in range(env.num_objectives):
    plt.plot(algo.returns[obj, :], label=selected_objective_names[obj])
plt.legend()
plt.xlabel("episode")
plt.ylabel("return")
plt.grid()
plt.show()

# plot some results
"""
fig, axs = plt.subplots(3, sharex=True)
axs[0].plot(algo.returns[0, :], label="movement penalty")
axs[1].plot(algo.returns[1, :], label="food")
axs[2].plot(algo.returns[2, :], label="water")
axs[2].set_xlabel("steps")
axs[1].set_ylabel("return")
axs[0].grid()
axs[1].grid()
axs[2].grid()
axs[0].legend()
axs[1].legend()
axs[2].legend()
plt.show()
"""