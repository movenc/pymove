import numpy as np
import matplotlib.pyplot as plt

import gym
import mo_gym

from pymove.approx_q_learning import QLearning
from pymove.exploration import EpsilonGreedy
from pymove.scalarizations import WeightedScalarization
from pymove.order_operators import ScalarizingOrderOperator
from pymove.approximation import TileCode
from pymove.env_wrappers import PastReturnAugmented, ObjectiveSelector

# environment
gamma = 1.0 # future discount factor (smaller value = more myopic agent)
gamma_past = 0.99 # past discount factor defines "memory capacity" of agent
unbreakable_bottles = False
objective_names = ["time penalty", "bottles delivered", "bottle drop penalty"]
selected_objectives = [0, 1, 2]
selected_objective_names = [objective_names[obj] for obj in selected_objectives]
# apply wrappers to 
env = PastReturnAugmented(ObjectiveSelector(gym.make("breakable-bottles-v0", unbreakable_bottles=unbreakable_bottles), selected_objectives), gamma_past)

# algorithm parameters
verbose = True
num_episodes = 5
alpha = 0.05 # learning rate
alpha_decay = 1.0
epsilon = 0.1 # exploration rate
epsilon_step = epsilon/num_episodes # linearly decay epsilon

# setup features map
iht_size = 4096
num_tilings = 8
tile_size = 1/num_tilings

def state_action_parser(s, a):
    ds_dict, w = s
    ds = env.unwrapped.get_obs_idx(ds_dict)
    dsa = ds + a*env.num_observations
    return w, np.array(dsa)

float_dim_bounds = np.array([env.observation_space["past_return"].low, env.observation_space["past_return"].high])
int_dim_bounds = np.array([[[0], [sp.n]] for sp_label, sp in env.observation_space["obs"].items()]).squeeze(axis=-1).T
tile_code = TileCode(num_tilings, float_dim_bounds, int_dim_bounds, state_action_parser)

# setup behavior policy
#utility_function = lambda x: x # linear 
utility_function = lambda x: -np.exp(-x) # MORE
#utility_function = lambda x: np.where(x < 0, 1 - np.exp(-x), np.log(1 + x*(x >= 0))) # explog
scalarization = WeightedScalarization(utility=utility_function)
order_op = ScalarizingOrderOperator(scalarization)
exploration_policy = EpsilonGreedy(env, epsilon=epsilon, decay_type="lin", decay_params={"decay_step": epsilon_step}, order_operation=order_op)

# run the learning algorithm
algo = QLearning(env,
            num_episodes,
            alpha,
            alpha_decay=alpha_decay,
            gamma=gamma,
            exploration_policy=exploration_policy,
            feature_map=tile_code)

algo.learn(verbose=verbose)

# plot some results
plt.figure()
for obj in range(env.num_objectives):
    plt.plot(algo.returns[obj, :], label=selected_objective_names[obj])
plt.legend()
plt.xlabel("episode")
plt.ylabel("return")
plt.grid()

from pymove.utils import gini_coefficient
plt.figure()
plt.plot(gini_coefficient(algo.returns[:, :].T))
plt.xlabel("episode")
plt.ylabel("gini coefficient")
plt.show()