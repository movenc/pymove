import numpy as np
import matplotlib.pyplot as plt

import gym
import mo_gym

from pymove.tabular_q_learning import TabularQLearning
from pymove.exploration import EpsilonGreedy
from pymove.scalarizations import WeightedScalarization
from pymove.order_operators import ScalarizingOrderOperator
from pymove.env_wrappers import ObjectiveSelector

# environment
gamma = 1.0 # future discount factor (smaller value = more myopic agent)
unbreakable_bottles = True
objective_names = ["time penalty", "bottles delivered", "bottle drop penalty"]
selected_objectives = [0, 2]
selected_objective_names = [objective_names[obj] for obj in selected_objectives]
# apply wrappers to 
env = ObjectiveSelector(gym.make("breakable-bottles-v0", unbreakable_bottles=unbreakable_bottles), selected_objectives)

# algorithm parameters
verbose = True
num_episodes = 1000
alpha = 0.05 # learning rate
alpha_decay = 1.0
epsilon = 0.1 # exploration rate
epsilon_decay = 0.999
epsilon_step = epsilon/num_episodes # linearly decay epsilon

# setup behavior policy
#utility_function = lambda x: x # linear 
utility_function = lambda x: -np.exp(-x) # MORE
#utility_function = lambda x: np.where(x < 0, 1 - np.exp(-x), np.log(1 + x*(x >= 0))) # explog
scalarization = WeightedScalarization(utility=utility_function)
order_op = ScalarizingOrderOperator(scalarization)
exploration_policy = EpsilonGreedy(env, epsilon=epsilon, decay_type="exp", decay_params={"decay_rate": epsilon_decay}, order_operation=order_op)

# run the learning algorithm
algo = TabularQLearning(env,
            num_episodes,
            alpha,
            alpha_decay=alpha_decay,
            gamma=gamma,
            exploration_policy=exploration_policy)

algo.learn(verbose=True)

# plot some results
plt.figure()
for obj in range(env.num_objectives):
    plt.plot(algo.returns[obj, :], label=selected_objective_names[obj])
plt.legend()
plt.xlabel("episode")
plt.ylabel("return")
plt.grid()
plt.show()