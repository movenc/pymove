import subprocess
try:    
    subprocess.call(["pip", "uninstall", "-y", "ai-safety-gridworlds"])
    subprocess.call(["pip", "uninstall", "-y", "safe-grid-gym"])
    subprocess.call(["pip", "uninstall", "-y", "mo-gym"])
    subprocess.call(["pip", "uninstall", "-y", "pymove"])
except:
    pass
subprocess.call(["pip", "install", "-e", "pymove/envs/safe-grid-gym"])
subprocess.call(["pip", "install", "-e", "pymove/envs/mo-gym"])
subprocess.call(["pip", "install", "-e", "."])
#"--upgrade", "--no-deps", "--force-reinstall"